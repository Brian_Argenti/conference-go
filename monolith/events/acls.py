from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_weather_data(city, state):
    loc_url = "http://api.openweathermap.org/geo/1.0/direct"
    url_params = {
        "appid": OPEN_WEATHER_API_KEY,
        "q": f"{city}, {state}, US",
    }

    response = requests.get(loc_url, params=url_params)
    local_data = json.loads(response.content)

    local = {
        "lat": local_data[0]["lat"],
        "lon": local_data[0]["lon"],
    }

    wx_url = "https://api.openweathermap.org/data/2.5/weather"
    wx_params = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": local["lat"],
        "lon": local["lon"],
    }

    wx_response = requests.get(wx_url, params=wx_params)
    local_wx = json.loads(wx_response.content)

    weather = {
        "temp": local_wx['main']['temp'],
        "weather": local_wx["weather"]
    }

    return weather


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": "map of " + city + " " + state, " per_page": 1}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    return { "picture_url": content["photos"][0]["src"]["original"] }
